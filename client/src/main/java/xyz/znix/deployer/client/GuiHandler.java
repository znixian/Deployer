package xyz.znix.deployer.client;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * A system to display windows, splashes and dialogs
 * <p>
 * Created by znix on 19/02/17.
 */
public class GuiHandler {
	private JWindow splash;

	public void showSplash(String image) {
		showSplash(getClass().getResource(image));
	}

	public void showSplash(URL url) {
		if (splash == null) setupSplash();

		Image image = Toolkit.getDefaultToolkit().createImage(url);
		Dimension scr = Toolkit.getDefaultToolkit().getScreenSize();
		ImagePanel ip = (ImagePanel) splash.getContentPane();

		ip.setImage(image);
		splash.pack();
		splash.setSize(ip.getPreferredSize());
		splash.setLocation(
				(int) (scr.getWidth() - splash.getWidth()) / 2,
				(int) (scr.getHeight() - splash.getHeight()) / 2
		);
		splash.setVisible(true);
	}

	public void hideSplash() {
		splash.setVisible(false);
	}

	private void setupSplash() {
		splash = new JWindow();
		splash.setAlwaysOnTop(true);
		splash.setContentPane(new ImagePanel());
	}

	public void readSettings(Settings settings) {
		if (settings.getBoolean("splash.enabled")) {
			showSplash(settings.get("splash.image"));
		}
	}

	private static class ImagePanel extends JPanel {
		private Image image;

		public Image getImage() {
			return image;
		}

		public void setImage(Image image) {
			this.image = image;

			MediaTracker tracker = new MediaTracker(this);
			tracker.addImage(image, 0);
			try {
				tracker.waitForAll();
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}

			repaint();
		}

		@Override
		public void paintComponent(Graphics g) {
			g.drawImage(image, 0, 0, this);
		}

		@Override
		public Dimension getPreferredSize() {
			if (image == null) return super.getPreferredSize();
			return new Dimension(image.getWidth(this), image.getHeight(this));
		}
	}
}
