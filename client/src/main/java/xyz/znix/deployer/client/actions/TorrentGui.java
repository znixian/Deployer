package xyz.znix.deployer.client.actions;

import com.frostwire.jlibtorrent.swig.torrent_handle;
import com.frostwire.jlibtorrent.swig.torrent_status;
import net.miginfocom.swing.MigLayout;
import xyz.znix.deployer.message.I18n;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * GUI window to display torrent progress.
 * <p>
 * Created by znix on 21/02/17.
 */
public class TorrentGui {
	private final JFrame frame;
	private final JPanel contentPane;

	/**
	 * The progress bar
	 */
	private JProgressBar progress;

	public TorrentGui(torrent_handle torrent) {
		frame = new JFrame("Torrent Progress");

		contentPane = new JPanel(new MigLayout(
				"",
				"[][grow]",
				""
		));

		addTorrent(torrent);

		frame.setContentPane(contentPane);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		// minimise (iconify) on close
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				frame.setExtendedState(JFrame.ICONIFIED);
			}
		});
		frame.setBounds(100, 100, 450, 450);
		frame.setVisible(true);

		// Open over everything else
		frame.toFront();
		frame.repaint();
	}

	private void addTorrent(torrent_handle torrent) {
		String name = torrent.status().getName();

		progress = new JProgressBar();
		progress.setMaximum(1000000); // 1,000,000 because torrent_handle.getProgressPPM() gives it in 0-1,000,000
		progress.setIndeterminate(true); // Until the first block is done
		progress.setStringPainted(false); // don't display any text

		contentPane.add(new JLabel(name));
		contentPane.add(progress, "wrap, growx");

		contentPane.revalidate();
		contentPane.repaint();
	}

	public void updateTorrent(torrent_handle torrent) {
		long id = torrent.id();
		torrent_status status = torrent.status();
		torrent_status.state_t state = status.getState();

		boolean seeding = id == torrent_status.state_t.seeding.swigValue();

		String text;
		if (!seeding) {
			text = String.format("%s %2.1f%%", state.toString(), status.getProgress() * 100);
		} else {
			text = I18n.get("torrent.status.complete");
		}

		progress.setIndeterminate(false);
		progress.setStringPainted(true);
		progress.setValue(status.getProgress_ppm());
		progress.setString(text);

		if (seeding) {
			frame.dispose();
		}
	}
}
