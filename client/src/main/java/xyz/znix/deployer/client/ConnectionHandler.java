package xyz.znix.deployer.client;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import xyz.znix.deployer.message.Message;

import java.util.*;

/**
 * Handle commands sent from the server.
 * <p>
 * Created by znix on 17/02/17.
 */
public class ConnectionHandler extends ChannelInboundHandlerAdapter {
	private final List<Message> unprocessedQueue;
	private final Set<Integer> processedIds;

	public ConnectionHandler() {
		unprocessedQueue = Collections.synchronizedList(new ArrayList<>());
		processedIds = Collections.synchronizedSet(new HashSet<>());
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object in) {
		Message m = (Message) in;
		unprocessedQueue.add(m);
		processQueue();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}

	private void processQueue() {

		// List of messages we can process, and do them outside
		// of the synchronized block.
		List<Message> toProcess = new ArrayList<>();

		synchronized (unprocessedQueue) {

			// filter out all the un-processable messages
			unprocessedQueue.stream()
					.filter(m -> m.dependenciesFulfilled(processedIds))
					.forEach(toProcess::add);

			// Don't want to process something twice
			unprocessedQueue.removeAll(toProcess);
		}

		for (Message message : toProcess) {

			// Make the 'done' callback. This marks the message as done,
			// and reruns processQueue in case any other messages depending
			// on it have been delivered.
			MessageMap.OrderProcessedHandler handler = () -> {
				processedIds.add(message.getId());
				processQueue();
			};

			// Run the message
			MessageMap.run(message, handler);
		}
	}
}
