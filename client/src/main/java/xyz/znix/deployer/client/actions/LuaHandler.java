package xyz.znix.deployer.client.actions;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.jse.JsePlatform;
import xyz.znix.deployer.client.MessageMap;
import xyz.znix.deployer.message.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.function.Function;

/**
 * Handler to deal with Lua-based anything
 * <p>
 * Created by znix on 17/03/17.
 */
public class LuaHandler {
	private final Globals globals;
	private int messageIndex = 0x3FFFFFFF; // half of int_max

	public LuaHandler() {

		// Build a helpful immutable metatable
		LuaTable immutable = new LuaTable();
		immutable.set("__newindex", onearg(arg -> {
			throw new RuntimeException("Immutable!");
		}));

		// Make the shared environment, somewhere mutable that scripts can use to communicate
		LuaTable sharedEnv = new LuaTable();

		// Set up the functions
		LuaTable deployer = new LuaTable();
		deployer.set("applyCommand", onearg(this::applyCommand));
		deployer.setmetatable(immutable);

		globals = JsePlatform.standardGlobals();
		globals.set("sharedEnv", sharedEnv);
		globals.set("deployer", deployer);
		globals.setmetatable(immutable);
	}

	public void execMessage(Message raw, MessageMap.OrderProcessedHandler done) {
		MsgRunLua msg = (MsgRunLua) raw;

		exec(msg.getProgram(), done);
	}

	public void exec(String lua, MessageMap.OrderProcessedHandler done) {
		LuaTable instOps = new LuaTable();
		instOps.set("done", onearg(v -> {
			done.done();
			return LuaValue.NIL;
		}));

		LuaValue chunk = globals.load(lua);
		chunk.call(instOps);
	}

	private LuaValue applyCommand(LuaValue data) {
		LuaTable params = data.checktable();

		// Find what this command depends on
		int dependency = params.get("dependency").optint(0);

		// Find the ID of this message
		int id = messageIndex++;

		// Build the meta data
		Message.MetaData metadata = new Message.MetaData(id, dependency);

		// Make the message
		Message message;
		String type = params.get("type").checkjstring();
		switch (type) {
			case "runCommand":
				message = makeRunCommand(metadata, params);
				break;
			case "downloadTorrent":
				message = makeDownloadTorrentCommand(metadata, params);
				break;
			case "splashScreen":
				message = makeSplashScreenCommand(metadata, params);
				break;
			default:
				throw new RuntimeException("Bad type " + type);
		}

		// Run the message
		MessageMap.run(message, () -> {
			LuaValue done = params.get("done");
			if (!done.isnil()) {
				done.checkfunction().invoke();
			}
		});

		// Return the ID of this message
		return LuaValue.valueOf(id);
	}

	private Message makeRunCommand(Message.MetaData data, LuaTable params) {
		return new MsgRunCommand(data, params.get("command").checkjstring());
	}

	private Message makeDownloadTorrentCommand(Message.MetaData data, LuaTable params) {
		String torrentFile = params.get("torrentFile").checkjstring();

		byte[] bytes;

		try {
			bytes = Files.readAllBytes(
					new File(torrentFile).toPath()
			);
		} catch (IOException ex) {
			throw new RuntimeException("Cannot load torrent file!", ex);
		}

		return new MsgDownloadTorrent(
				data,
				bytes,
				params.get("outdir").checkjstring()
		);
	}

	private Message makeSplashScreenCommand(Message.MetaData data, LuaTable params) {
		return new MsgSplashScreen(
				data,
				params.get("address").optjstring(""),
				MsgSplashScreen.Mode.valueOf(params.get("mode").checkjstring().toUpperCase())
		);
	}

	private Message makeRunLua(Message.MetaData data, LuaTable params) {
		return new MsgRunLua(data, params.get("script").checkjstring());
	}

	private static OneArgFunction onearg(Function<LuaValue, LuaValue> func) {
		return new OneArgFunction() {
			@Override
			public LuaValue call(LuaValue arg) {
				return func.apply(arg);
			}
		};
	}
}
