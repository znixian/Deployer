package xyz.znix.deployer.client.actions;

import com.frostwire.jlibtorrent.LibTorrent;
import com.frostwire.jlibtorrent.Vectors;
import com.frostwire.jlibtorrent.alerts.Alert;
import com.frostwire.jlibtorrent.alerts.AlertType;
import com.frostwire.jlibtorrent.alerts.Alerts;
import com.frostwire.jlibtorrent.alerts.TorrentAlert;
import com.frostwire.jlibtorrent.swig.*;
import xyz.znix.deployer.client.LibTorrentLoader;

import java.io.File;
import java.util.*;
import java.util.stream.Stream;

/**
 * A wrapper around libtorrent's session object.
 * <p>
 * Created by znix on 20/02/17.
 */
public class TorrentHandler {
	private static final int POLL_SLEEP_TIME = 500; // 500ms between polling the alert status

	private final Map<ListenerTarget, List<AlertListener>> listeners;
	private final session session;

	public TorrentHandler() {
		// Set up the listeners map
		listeners = new HashMap<>();
		for (ListenerTarget type : ListenerTarget.values()) {
			listeners.put(type, new ArrayList<>());
		}

		// Load the native library file
		LibTorrentLoader.load();

		// Print out the libtorrent version, just in case...
		// TODO evaluate if this is necessary, then remove the print or this todo
		System.out.println("Using libtorrent version: " + LibTorrent.version());

		// Set up the session
		settings_pack settings = new settings_pack();

		// disable everything
		settings.set_bool(settings_pack.bool_types.enable_upnp.swigValue(), false);
		settings.set_bool(settings_pack.bool_types.enable_natpmp.swigValue(), false);
		settings.set_bool(settings_pack.bool_types.enable_dht.swigValue(), false);

		// turn on LSD (Local Seed Discovery - finding seeds/peers on the LAN)
		settings.set_bool(settings_pack.bool_types.enable_lsd.swigValue(), true);

		// Disable uTP. Having it on royally fucks things up (they can't download from each other)
		settings.set_bool(settings_pack.bool_types.enable_incoming_utp.swigValue(), false);
		settings.set_bool(settings_pack.bool_types.enable_outgoing_utp.swigValue(), false);

		// Set the alert mask
		settings.set_int(settings_pack.int_types.alert_mask.swigValue(), getPrintMask());

		// Set the client fingerprint
		String fingerprint = libtorrent.generate_fingerprint("dp", 1, 0, 0, 0);
		while (fingerprint.length() < 20) {
			fingerprint += (char) (Math.random() * 255);
		}
		settings.set_str(settings_pack.string_types.peer_fingerprint.swigValue(), fingerprint);

		session = new session(settings);
	}

	// TODO: addAlertListener, but for a specific torrent

	/**
	 * Add an alert listener. It will be called when an event takes place.
	 *
	 * @param type     The type of alert to listen for, or {@code null} for all events.
	 * @param listener The listener to call.
	 */
	public void addAlertListener(ListenerTarget type, AlertListener listener) {
		getListenerStreams(type).forEach(l -> l.add(listener));
	}

	/**
	 * Remove an alert listener.
	 *
	 * @param type     The type of alert to remove the listener from, or {@code null} for all events.
	 * @param listener The listener to remove.
	 */
	public void removeAlertListener(ListenerTarget type, AlertListener listener) {
		getListenerStreams(type).forEach(l -> l.remove(listener));
	}

	private void postAlert(ListenerTarget type, torrent_handle torrent) {
		// Make a temporary array, so we don't get ConcurrentModificationException-s
		// if the listeners remove themselves.
		List<AlertListener> tmp = new ArrayList<>(listeners.get(type));

		tmp.forEach(l -> l.alert(type, torrent));
	}

	private Stream<List<AlertListener>> getListenerStreams(ListenerTarget type) {
		if (type == null) {
			return listeners.values().stream();
		} else {
			return listeners.entrySet().stream()
					.filter(e -> e.getKey() == type)
					.map(Map.Entry::getValue);
		}
	}

	public torrent_info fromBytes(byte[] data) {
		byte_vector buffer = Vectors.bytes2byte_vector(data);
		bdecode_node n = new bdecode_node();
		error_code ec = new error_code();
		int ret = bdecode_node.bdecode(buffer, n, ec);

		if (ret != 0)
			throw new IllegalArgumentException("Can't decode data: " + ec.message());

		ec.clear();
		torrent_info info = new torrent_info(n, ec);
		buffer.clear(); // prevents GC
		if (ec.value() != 0)
			throw new IllegalArgumentException("Can't decode data: " + ec.message());

		return info;
	}

	public torrent_handle addTorrent(File target, torrent_info info) {
		{
			String newhex = info.info_hash().to_hex();
			torrent_handle_vector torrents = session.get_torrents();
			for (int i = 0; i < torrents.size(); i++) {
				torrent_handle th = torrents.get(i);
				String hex = th.info_hash().to_hex();

				if (newhex.equals(hex)) {
					// TODO should this be ignored and/or trackers merged, or throw an exception?
					throw new IllegalStateException("The torrent " + info.name() + " is already present!");
				}
			}
		}

		if (target.isFile()) {
			throw new IllegalArgumentException("Target directory " + target + " is a file!");
		} else if (!target.isDirectory()) {
			boolean mkdirs = target.mkdirs();
			if (!mkdirs) {
				throw new RuntimeException("Failed to create output directory " + target);
			}
		}

		add_torrent_params p = add_torrent_params.create_instance();
		p.setSave_path(target.getAbsolutePath());
		p.set_ti(info);

		error_code err = new error_code();

		return session.add_torrent(p, err);
	}

	public void startLoopThread() {
		Thread thread = new Thread(this::torrentLoop);
		thread.setName("TorrentHandler-Alert-Polling");
		thread.start();
	}

	@SuppressWarnings("InfiniteLoopStatement")
	private void torrentLoop() {
		Set<Long> progressSent = new HashSet<>();

		while (true) {
			progressSent.clear();

			alert_ptr_vector alerts = new alert_ptr_vector();
			session.pop_alerts(alerts);

			for (int i = 0; i < alerts.size(); i++) {
				alert a = alerts.get(i);

				Alert<?> alertJava = Alerts.cast(a);
				AlertType type = AlertType.fromSwig(a.type());

//				System.out.println(type);
				switch (type) {
					case PEER_CONNECT:
					case PEER_DISCONNECTED:
					case TRACKER_ANNOUNCE:
					case TRACKER_REPLY:
						System.out.println(alertJava);
				}

				if (alertJava instanceof TorrentAlert) {
					torrent_handle torrent = ((TorrentAlert) alertJava).handle().swig();

					switch (type) {
						case TORRENT_FINISHED:
							postAlert(ListenerTarget.FINISHED, torrent);
							break;
						case TORRENT_ERROR:
							postAlert(ListenerTarget.ERROR, torrent);
							break;
						case BLOCK_FINISHED: {
							if (canMassUpdate(progressSent, torrent))
								postAlert(ListenerTarget.PROGRESS, torrent);
							break;
						}
					}
				}

			}
			try {
				Thread.sleep(POLL_SLEEP_TIME); // TODO investigate wait_for_alert_ms
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private boolean canMassUpdate(Set<Long> set, torrent_handle torrent) {
		long id = torrent.id();
		if (!set.contains(id)) {
			set.add(id);
			return true;
		} else {
			return false;
		}
	}

	private static int getPrintMask() {
		int mask = alert.category_t.all_categories.swigValue();

		int log_mask = alert.category_t.session_log_notification.swigValue() |
				alert.category_t.torrent_log_notification.swigValue() |
				alert.category_t.peer_log_notification.swigValue() |
				alert.category_t.dht_log_notification.swigValue() |
				alert.category_t.port_mapping_log_notification.swigValue() |
				alert.category_t.picker_log_notification.swigValue();
		mask = mask & ~log_mask;

		return mask;
	}

	public enum ListenerTarget {
		PROGRESS, FINISHED, ERROR
	}

	public interface AlertListener {
		void alert(ListenerTarget type, torrent_handle torrent);
	}
}
