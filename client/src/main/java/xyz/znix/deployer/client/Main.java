package xyz.znix.deployer.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import xyz.znix.deployer.client.actions.LuaStartupHandler;

import java.io.IOException;

/**
 * Main class for the client.
 * <p>
 * Created by znix on 17/02/17.
 */
public class Main {
	private static Main instance;

	private final Settings settings;
	private final GuiHandler gui;

	public static void main(String... args) throws InterruptedException, IOException {
		new Main();
	}

	public static Main getInstance() {
		return instance;
	}

	private Main() throws IOException, InterruptedException {
		instance = this;
		settings = new Settings();
		gui = new GuiHandler();
		gui.readSettings(settings);

		LuaStartupHandler.startup(settings);

		String host = settings.get("host.address");
		int port = settings.getInt("host.port");

		EventLoopGroup workerGroup = new NioEventLoopGroup();

		try {
			Bootstrap b = new Bootstrap(); // (1)
			b.group(workerGroup); // (2)
			b.channel(NioSocketChannel.class); // (3)
			b.option(ChannelOption.SO_KEEPALIVE, true); // (4)
			b.handler(new ChannelInitializer<SocketChannel>() {
				@Override
				public void initChannel(SocketChannel ch) throws Exception {
					ch.pipeline().addLast(
							new OrderDecoder(),
							new ConnectionHandler()
					);
				}
			});

			// Start the client.
			ChannelFuture f = b.connect(host, port).sync(); // (5)

			// Wait until the connection is closed.
			f.channel().closeFuture().sync();
		} finally {
			workerGroup.shutdownGracefully();

			// Actually quit, even if AWT/Swing is keeping everything open
			System.exit(0);
		}
	}

	public Settings getSettings() {
		return settings;
	}

	public GuiHandler getGui() {
		return gui;
	}
}
