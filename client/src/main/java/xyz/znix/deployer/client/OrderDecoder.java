package xyz.znix.deployer.client;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import xyz.znix.deployer.message.Message;
import xyz.znix.deployer.message.MessageType;

import java.util.List;

/**
 * Decodes an order (in ByteBuf) to a Message object.
 * <p>
 * Created by znix on 17/02/17.
 */
public class OrderDecoder extends ByteToMessageDecoder {
	private static final int PRELEN_HEADER_LENGTH = 12; // id-int, type-int, dependency-int
	private static final int HEADER_LENGTH = PRELEN_HEADER_LENGTH + 4;

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
		if (in.readableBytes() < HEADER_LENGTH) {
			return;
		}

		int length = in.getInt(in.readerIndex() + PRELEN_HEADER_LENGTH);

		if (in.readableBytes() < length + HEADER_LENGTH) {
			return;
		}

		int id = in.readInt();
		int typeId = in.readInt();
		int depId = in.readInt();

		Message.MetaData md = new Message.MetaData(id);
		md.setDependency(depId);

		MessageType type = MessageType.values()[typeId];
		Message message = type.getInit().read(
				md,
				in.readInt(),
				in
		);

		out.add(message);
	}
}
