package xyz.znix.deployer.client.actions;

import org.apache.commons.io.IOUtils;
import xyz.znix.deployer.client.Settings;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Created by znix on 17/03/17.
 */
public class LuaStartupHandler {
	private LuaStartupHandler() {
	}

	public static void startup(Settings settings) throws IOException {
		String scriptFile = settings.get("lua.autorun");
		if (scriptFile == null) return;

		String lua;
		try (InputStream br = LuaStartupHandler.class.getResourceAsStream(scriptFile)) {
			lua = IOUtils.toString(br, Charset.forName("UTF-8"));
		}

		LuaHandler startupHandler = new LuaHandler();
		startupHandler.exec(lua, null);
	}
}
