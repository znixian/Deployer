package xyz.znix.deployer.client;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Read and store the settings from the config file.
 * <p>
 * <p>
 * Created by znix on 18/02/17.
 */
public class Settings {
	private final Properties properties;

	public Settings() throws IOException {
		InputStream in = getClass().getResourceAsStream("/config.properties");

		properties = new Properties();
		properties.load(in);

		verify("host.address");
		verify("host.port");
		verify("splash.enabled");
		verify("splash.image");
		verify("torrent.gui.enabled");
	}

	private void verify(String key) {
		if (get(key) == null) {
			throw new IllegalStateException("Missing key " + key);
		}
	}

	public Properties getProperties() {
		return properties;
	}

	public String get(String key) {
		return getProperties().getProperty(key);
	}

	public int getInt(String key) {
		return Integer.parseInt(get(key));
	}

	public boolean getBoolean(String key) {
		return Boolean.parseBoolean(get(key));
	}
}
