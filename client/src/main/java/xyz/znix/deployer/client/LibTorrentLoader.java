package xyz.znix.deployer.client;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.util.Arrays;

/**
 * Loads the native library for jLibTorrent
 * <p>
 * Created by znix on 17/02/17.
 */
public class LibTorrentLoader {
	private LibTorrentLoader() {
	}

	private static boolean loaded;

	public static boolean isWindows() {
		return System.getProperty("os.name").contains("Windows");
	}

	public static void load() {
		if (loaded) return;
		loaded = true;

		try {
			String arch = System.getProperty("os.arch").contains("64") ? "x86_64" : "x86";
			String filename = isWindows() ? "jlibtorrent.dll" : "libjlibtorrent.so";

			String path = "/lib/" + arch + "/" + filename;

			InputStream in = LibTorrentLoader.class.getResourceAsStream(path);

			System.out.println("Loading native library " + path);

			File tempDir = Files.createTempDirectory("jlibtorrent-").toFile();

			Runtime.getRuntime().addShutdownHook(new Thread(() -> {
				for (File listFile : tempDir.listFiles()) {
					listFile.delete();
				}
				tempDir.delete();
			}));

			File outFile = new File(tempDir, filename);

			OutputStream out = FileUtils.openOutputStream(outFile);
			IOUtils.copy(in, out);

			in.close();
			out.close();

			addLibraryPath(tempDir.getAbsolutePath());
		} catch (IOException | IllegalAccessException | NoSuchFieldException e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 * Adds the specified path to the java library path
	 *
	 * @param pathToAdd the path to add
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 * @link http://fahdshariff.blogspot.co.nz/2011/08/changing-java-library-path-at-runtime.html
	 */
	private static void addLibraryPath(String pathToAdd) throws NoSuchFieldException, IllegalAccessException {
		final Field usrPathsField = ClassLoader.class.getDeclaredField("usr_paths");
		usrPathsField.setAccessible(true);

		//get array of paths
		final String[] paths = (String[]) usrPathsField.get(null);

		//check if the path to add is already present
		for (String path : paths) {
			if (path.equals(pathToAdd)) {
				return;
			}
		}

		//add the new path
		final String[] newPaths = Arrays.copyOf(paths, paths.length + 1);
		newPaths[newPaths.length - 1] = pathToAdd;
		usrPathsField.set(null, newPaths);
	}
}
