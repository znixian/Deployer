package xyz.znix.deployer.client;

import com.frostwire.jlibtorrent.swig.torrent_handle;
import xyz.znix.deployer.client.actions.LuaHandler;
import xyz.znix.deployer.client.actions.TorrentGui;
import xyz.znix.deployer.client.actions.TorrentHandler;
import xyz.znix.deployer.message.*;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Maps out client-specific actions for different messages
 * <p>
 * Created by znix on 17/02/17.
 */
public class MessageMap {
	private static final Map<MessageType, OrderProcessor> runners;

	private static TorrentHandler controller;
	private static LuaHandler lua;

	static {
		runners = new HashMap<>();
		runners.put(MessageType.RUN_COMMAND, MessageMap::runCommand);
		runners.put(MessageType.TORRENT, MessageMap::downloadTorrent);
		runners.put(MessageType.SPLASH_SCREEN, MessageMap::splashScreen);
		runners.put(MessageType.RUN_LUA, MessageMap::runLua);
	}

	/**
	 * Runs a given message
	 *
	 * @param message The message to run
	 */
	public static void run(Message message, OrderProcessedHandler handler) {
		MessageType type = message.getType();
		OrderProcessor msc = runners.get(type);
		msc.process(message, handler);
	}

	private MessageMap() {
	}

	private static void debug(Message raw) {
		System.out.println("DEBUG: Received " + raw);
	}

	////////////////////////////

	private static void runCommand(Message raw, OrderProcessedHandler done) {
		runInExternThread(MessageMap::runCommandSlow, raw, done);
	}

	private static void runCommandSlow(Message raw, OrderProcessedHandler done) {
		MsgRunCommand cmd = (MsgRunCommand) raw;
		try {
			Process exec = Runtime.getRuntime().exec(cmd.getCommand());
			System.out.println("Return " + exec.waitFor() + " for " + cmd.getCommand());

			// byte[] data = new byte[exec.getInputStream().available()];
			// exec.getInputStream().read(data);
			// System.out.println(new String(data));

			done.done();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static void downloadTorrent(Message raw, OrderProcessedHandler callback) {
		MsgDownloadTorrent msg = (MsgDownloadTorrent) raw;
		Settings settings = Main.getInstance().getSettings();

		if (controller == null) {
			controller = new TorrentHandler();
			controller.startLoopThread();
		}

		torrent_handle torrent = controller.addTorrent(
				new File(msg.getOutdir()),
				controller.fromBytes(msg.getTorrent())
		);

		// Setup the GUI
		final TorrentGui gui;
		if (settings.getBoolean("torrent.gui.enabled")) {
			gui = new TorrentGui(torrent);
		} else {
			gui = null;
		}

		// Add listeners:

		// Use an atomic reference, since lambdas cannot use 'this' to refer to themselves.
		AtomicReference<TorrentHandler.AlertListener> progressListener = new AtomicReference<>();

		progressListener.set((type, target) -> {
			if (target.id() != torrent.id()) return;

			if (gui != null) gui.updateTorrent(torrent);

			int p = (int) (torrent.status().getProgress() * 100);
//			System.out.println("Progress: " + p + "% for torrent name: " + torrent.status().getName());
		});

		controller.addAlertListener(TorrentHandler.ListenerTarget.PROGRESS, progressListener.get());

		// Add completion/error listener
		AtomicReference<TorrentHandler.AlertListener> completionListener = new AtomicReference<>();

		completionListener.set((type, target) -> {
			if (gui != null) gui.updateTorrent(torrent);

//			System.out.println("Torrent event: " + type + " for torrent " + torrent.status().getName());

			controller.removeAlertListener(null, completionListener.get());
			controller.removeAlertListener(null, progressListener.get());

			// TODO: is it appropriate to call done() when the torrent has errored?
			callback.done();
		});

		controller.addAlertListener(TorrentHandler.ListenerTarget.ERROR, completionListener.get());
		controller.addAlertListener(TorrentHandler.ListenerTarget.FINISHED, completionListener.get());
	}

	private static void splashScreen(Message raw, OrderProcessedHandler done) {
		MsgSplashScreen msg = (MsgSplashScreen) raw;
		GuiHandler gui = Main.getInstance().getGui();

		if (msg.getMode() != MsgSplashScreen.Mode.HIDE) {
			URL url;
			try {
				switch (msg.getMode()) {
					case FILE:
						url = new File(msg.getAddress()).toURI().toURL();
						break;
					case URL:
						url = new URL(msg.getAddress());
						break;
					case RESOURCE:
						url = gui.getClass().getResource(msg.getAddress());
						break;
					default:
						throw new RuntimeException("Unknown mode " + msg.getMode());
				}
			} catch (MalformedURLException e) {
				throw new RuntimeException(e);
			}

			gui.showSplash(url);
		} else {
			gui.hideSplash();
		}

		done.done();
	}

	private static void runLua(Message raw, OrderProcessedHandler done) {
		if (lua == null) lua = new LuaHandler();

		runInExternThread(lua::execMessage, raw, done);
	}

	private static void runInExternThread(OrderProcessor p, Message message, OrderProcessedHandler done) {
		Thread t = new Thread(() -> p.process(message, done));
		t.start();
	}

	private interface OrderProcessor {
		void process(Message message, OrderProcessedHandler done);
	}

	public interface OrderProcessedHandler {
		void done();
	}
}
