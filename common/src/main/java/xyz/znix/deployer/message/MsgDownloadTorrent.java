package xyz.znix.deployer.message;

import io.netty.buffer.ByteBuf;

/**
 * A message telling a client to download an attached torrent file.
 * <p>
 * Created by znix on 17/02/17.
 */
public class MsgDownloadTorrent extends Message {
	private final byte[] torrent;
	private final String outdir;

	public MsgDownloadTorrent(MetaData data, byte[] torrent, String outdir) {
		super(data);
		this.torrent = torrent;
		this.outdir = outdir;
	}

	@Override
	public MessageType getType() {
		return MessageType.TORRENT;
	}

	@Override
	public void write(ByteBuf data) {
		data.writeInt(
				4 + // Torrent length int
						torrent.length + // Torrent data
						4 + // Outdir length
						outdir.length() // Outdir data
		);

		data.writeInt(torrent.length);
		data.writeBytes(torrent);

		data.writeInt(outdir.length());
		data.writeBytes(outdir.getBytes());
	}

	static Message read(MetaData md, int length, ByteBuf data) {
		byte[] bytes = new byte[data.readInt()];
		data.readBytes(bytes);

		byte[] outdir = new byte[data.readInt()];
		data.readBytes(outdir);

		return new MsgDownloadTorrent(md, bytes, new String(outdir));
	}

	public byte[] getTorrent() {
		return torrent;
	}

	public String getOutdir() {
		return outdir;
	}
}
