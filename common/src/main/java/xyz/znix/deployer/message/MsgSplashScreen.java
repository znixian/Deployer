package xyz.znix.deployer.message;

import io.netty.buffer.ByteBuf;

/**
 * Modifies the splash screen on the client
 * <p>
 * Created by znix on 19/02/17.
 */
public class MsgSplashScreen extends Message {
	private final String address;
	private final Mode mode;

	public MsgSplashScreen(MetaData data, String address, Mode mode) {
		super(data);
		this.address = address;
		this.mode = mode;
	}

	@Override
	public MessageType getType() {
		return MessageType.SPLASH_SCREEN;
	}

	@Override
	public void write(ByteBuf data) {
		data.writeInt(address.length() + 4); // Length
		data.writeInt(mode.ordinal());
		data.writeBytes(address.getBytes());
	}

	static MsgSplashScreen read(MetaData md, int length, ByteBuf data) {
		Mode mode = Mode.values()[data.readInt()];

		byte[] bytes = new byte[length - 4];
		data.readBytes(bytes);

		return new MsgSplashScreen(md, new String(bytes), mode);
	}

	public String getAddress() {
		return address;
	}

	public Mode getMode() {
		return mode;
	}

	public enum Mode {
		HIDE, RESOURCE, FILE, URL
	}
}
