package xyz.znix.deployer.message;

import io.netty.buffer.ByteBuf;

/**
 * A message telling a client to run a command.
 * <p>
 * Created by znix on 17/02/17.
 */
public class MsgRunCommand extends Message {
	private final String command;

	public MsgRunCommand(MetaData data, String command) {
		super(data);
		this.command = command;
	}

	@Override
	public MessageType getType() {
		return MessageType.RUN_COMMAND;
	}

	@Override
	public void write(ByteBuf data) {
		data.writeInt(command.length()); // Length
		data.writeBytes(command.getBytes());
	}

	public String getCommand() {
		return command;
	}

	static Message read(MetaData md, int length, ByteBuf data) {
		byte[] bytes = new byte[length];
		data.readBytes(bytes);

		String cmd = new String(bytes);

		return new MsgRunCommand(md, cmd);
	}
}
