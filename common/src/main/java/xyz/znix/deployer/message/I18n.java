package xyz.znix.deployer.message;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Internationalization helper.
 * <p>
 * Created by znix on 27/02/17.
 */
public class I18n {
	private static final ResourceBundle messages;
	private static final ResourceBundle messages2;

	static {
		Locale locale = Locale.getDefault();

		messages = ResourceBundle.getBundle("MessagesBundle", locale);
		messages2 = ResourceBundle.getBundle("MessagesBundleCommon", locale);
	}

	public static String get(String key) {
		if (messages.containsKey(key)) return messages.getString(key);
		if (messages2.containsKey(key)) return messages2.getString(key);

		throw new IllegalArgumentException("No such key " + key);
	}

	public static String get(String key, Object... objects) {
		return String.format(get(key), objects);
	}
}
