package xyz.znix.deployer.message;

import io.netty.buffer.ByteBuf;

import java.util.Set;

/**
 * Represents a individual message.
 * <p>
 * Created by znix on 17/02/17.
 */
public abstract class Message {
	/**
	 * Unique ID for this message.
	 */
	private final int id;

	/**
	 * The ID of the order this message is dependant on, or -1 if it should be
	 * run as soon as it is received.
	 */
	private int dependency;

	public Message(MetaData data) {
		this.id = data.getId();
		this.dependency = data.getDependency();
	}

	public int getId() {
		return id;
	}

	public abstract MessageType getType();

	public int getDependency() {
		return dependency;
	}

	public void setDependency(int dependency) {
		this.dependency = dependency;
	}

	/**
	 * Write this message to the server.
	 * <p>
	 * First, the length of the message should be written. Next, any desired data can be written.
	 */
	public abstract void write(ByteBuf data);

	/**
	 * Returns whether or not the dependnecies for this message have been fulfilled.
	 *
	 * @param processedIds The list of the IDs of the processed messages.
	 * @return {@code true} if the order can be run, {@code false} otherwise.
	 */
	public boolean dependenciesFulfilled(Set<Integer> processedIds) {
		return getDependency() == -1 || processedIds.contains(getDependency());
	}

	@Override
	public String toString() {
		return super.toString() + " (" + getId() + "/" + getDependency() + ")";
	}

	public static class MetaData {
		private final int id;
		private int dependency;

		public MetaData(int id) {
			this.id = id;
		}

		public MetaData(int id, int dependency) {
			this.id = id;
			this.dependency = dependency;
		}

		public int getId() {
			return id;
		}

		public int getDependency() {
			return dependency;
		}

		public void setDependency(int dependency) {
			this.dependency = dependency;
		}
	}
}
