package xyz.znix.deployer.message;

import io.netty.buffer.ByteBuf;

/**
 * A message telling a client to run a program.
 * <p>
 * Created by znix on 17/02/17.
 */
public class MsgRunLua extends Message {
	private final String program;

	public MsgRunLua(MetaData data, String program) {
		super(data);
		this.program = program;
	}

	@Override
	public MessageType getType() {
		return MessageType.RUN_LUA;
	}

	@Override
	public void write(ByteBuf data) {
		data.writeInt(program.length()); // Length
		data.writeBytes(program.getBytes());
	}

	public String getProgram() {
		return program;
	}

	static Message read(MetaData md, int length, ByteBuf data) {
		byte[] bytes = new byte[length];
		data.readBytes(bytes);

		String cmd = new String(bytes);

		return new MsgRunLua(md, cmd);
	}
}
