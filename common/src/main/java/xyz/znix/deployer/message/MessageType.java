package xyz.znix.deployer.message;

import io.netty.buffer.ByteBuf;

/**
 * Represents the different types of messages.
 * <p>
 * Created by znix on 17/02/17.
 */
public enum MessageType {
	RUN_COMMAND(MsgRunCommand::read),
	TORRENT(MsgDownloadTorrent::read),
	SPLASH_SCREEN(MsgSplashScreen::read),
	RUN_LUA(MsgRunLua::read);

	private final Deserializer init;
	private final String niceName;

	MessageType(Deserializer init) {
		this.init = init;
		this.niceName = I18n.get("message." + name().toLowerCase());
	}

	public Deserializer getInit() {
		return init;
	}

	public String getNiceName() {
		return niceName;
	}

	public interface Deserializer {
		Message read(Message.MetaData md, int length, ByteBuf data);
	}
}
