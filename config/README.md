# Configuration

The configs for both the server and client are
contained in this directory.

## Client configuration
Put any client side configuration stuff into a file
called config.properties

This is in the same format as the default config file
at client/src/main/resources/config.properties

All options:

`host.address` Default: `0.0.0.0` The address of the host to connect to

`host.port` Default: `1234` The port to connect on

`splash.enabled` Default: `true` Should a splash screen be shown by default?

`splash.image` Default: `/example-splash.png` The image of the default splash
screen, if `splash.enabled` is set to `true`. The path should be in the format
of `/filename`, where filename is the name of a image file placed into this
directory

`torrent.gui.enabled` Default: `true` When a torrent starts downloading, should
a GUI that shows the progress appear?

`lua.autorun` Default: None. The resource name of a lua file that should be run
when the client starts, before the client connects to the server.
This setting is optional - if you don't want anything
special to happen when the program opens, omit this option.
Example: `lua.autorun=/example.lua`

Example file, setting default values:
```
host.address=0.0.0.0
host.port=1234

splash.enabled=true
splash.image=/example-splash.png

torrent.gui.enabled=true
```

## Server Configuration
Currently, just put the required port into
a file called 'portspec' in this directory.

Example, runnable from the project root:
```
echo "12345" > config/portspec
```

Set the port to 12345
