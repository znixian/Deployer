package xyz.znix.deployer.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.concurrent.GlobalEventExecutor;
import xyz.znix.deployer.server.gui.GuiMain;
import xyz.znix.deployer.server.gui.IGuiServerIO;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Main class for the server.
 * <p>
 * Created by znix on 17/02/17.
 */
public class Main implements IGuiServerIO {
	private final GuiMain gui;
	private final ChannelGroup clients;

	public static void main(String... args) throws Exception {
		new Main();
	}

	private Main() throws Exception {
		gui = new GuiMain(this);
		clients = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

		int port;
		try (BufferedReader br = new BufferedReader(new InputStreamReader(
				getClass().getResourceAsStream("/portspec")))) {
			String line = br.readLine();
			port = Integer.parseInt(line);
		}

		EventLoopGroup bossGroup = new NioEventLoopGroup(); // Accepts connections
		EventLoopGroup workerGroup = new NioEventLoopGroup(); // Handles connections after bossGroup has accepted them
		try {
			ServerBootstrap b = new ServerBootstrap(); // (2)
			b.group(bossGroup, workerGroup)
					.channel(NioServerSocketChannel.class) // (3)
					.childHandler(new ChannelInitializer<SocketChannel>() { // (4)
						@Override
						public void initChannel(SocketChannel ch) throws Exception {
							// Outbound Adapters
							ch.pipeline().addLast(
									new MessageEncoder(),
									new OrderToMessageEncoder()
							);

							// Inbound Adapters
							ch.pipeline().addLast(
									new ConnectingClientHandler(gui.getInboundOrderProvider(), clients)
							);
						}
					})
					.option(ChannelOption.SO_BACKLOG, 128)          // (5)
					.childOption(ChannelOption.SO_KEEPALIVE, true); // (6)

			// Bind and start to accept incoming connections.
			ChannelFuture f = b.bind(port).sync(); // (7)

			// Start the GUI
			gui.start();

			// Wait until the server socket is closed.
			// In this example, this does not happen, but you can do that to gracefully
			// shut down your server.
			f.channel().closeFuture().sync();
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}

	@Override
	public void sendToAllClients(Order order) {
		clients.writeAndFlush(order);
	}
}
