package xyz.znix.deployer.server.gui;

import xyz.znix.deployer.server.Order;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.function.Function;

/**
 * Represents a list of items, the label above it, and drag-and-drop support.
 * <p>
 * Created by znix on 17/02/17.
 */
public class ItemList extends JPanel {
	public static final DataFlavor ORDER_FLAVOR;

	static {
		try {
			String clazz = Order.class.getName();
			String mime = DataFlavor.javaJVMLocalObjectMimeType + ";class=" + clazz;
			ORDER_FLAVOR = new DataFlavor(mime);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private final DefaultListModel<Order> model;
	private final JList<Order> list;

	public ItemList(String name, ItemListMode mode, Function<Order, List<Order>> orderSupplier, Runnable repainter) {
		setLayout(new BorderLayout());

		add(new JLabel(name), BorderLayout.NORTH);

		model = new DefaultListModel<>();
		list = new JList<>(model);
		list.setCellRenderer(new OrderRenderer());
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				// Double-click detected
				if (evt.getClickCount() == 2) {
					JList list = (JList) evt.getSource();
					int index = list.locationToIndex(evt.getPoint());
					Order order = model.get(index);

					new OrderEditor(order, o -> repainter.run(), orderSupplier);
				}
			}
		});
		JScrollPane jsc = new JScrollPane(list);
		add(jsc, BorderLayout.CENTER);

		mode.setup(this);
	}

	public DefaultListModel<Order> getModel() {
		return model;
	}

	public void addOrder(Order order) {
		// Remove the element if it's already there, preventing duplicates
		model.removeElement(order);

		// Add the element
		model.addElement(order);
	}

	public JList<Order> getList() {
		return list;
	}
}
