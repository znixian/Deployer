package xyz.znix.deployer.server.gui;

import xyz.znix.deployer.server.Order;

import java.util.List;

/**
 * Provides orders to, for example, newly-connected clients.
 * <p>
 * Created by znix on 19/02/17.
 */
public interface IOrderProvider {

	/**
	 * Gets the list of orders to be sent to these clients.
	 *
	 * @return A list of orders to be sent to clients, or {@code null} if there are none.
	 */
	List<Order> getOrders();
}
