package xyz.znix.deployer.server.gui;

import xyz.znix.deployer.server.Order;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * Represents an order while being dragged.
 * <p>
 * Created by znix on 17/02/17.
 */
public class OrderSelection implements Transferable {
	private static final DataFlavor[] flavors = {ItemList.ORDER_FLAVOR};
	private final Order order;

	public OrderSelection(Order order) {
		this.order = order;
	}

	@Override
	public DataFlavor[] getTransferDataFlavors() {
		return flavors;
	}

	@Override
	public boolean isDataFlavorSupported(DataFlavor flavor) {
		return flavor.equals(ItemList.ORDER_FLAVOR);
	}

	@Override
	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
		return order;
	}
}
