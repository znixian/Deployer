package xyz.znix.deployer.server;

import xyz.znix.deployer.message.Message;
import xyz.znix.deployer.message.MessageType;
import xyz.znix.deployer.message.MsgSplashScreen;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Represents an order on the server, encapsulating a {@link Message}.
 * <p>
 * An order allows access to some server size-only functionality for MessageType: properties.
 * A MessageType can have property types associated with it. You can get these with {@link #getPropertyTypes()}.
 * TODO: finish docs
 * <p>
 * Created by znix on 17/02/17.
 */
public class Order {
	private static final Map<MessageType, Map<String, TypeInfo>> PROPERTIES;
	private static int GLOBAL_ID_CNTR = 1;

	static {
		Map<MessageType, Map<String, TypeInfo>> props = new HashMap<>();

		buildImmutable(props, MessageType.RUN_COMMAND, m -> m.put("command", Type.STRING.info()));
		buildImmutable(props, MessageType.TORRENT, m -> {
			m.put("torrent", Type.FILE.info());
			m.put("outdir", Type.STRING.info());
		});
		buildImmutable(props, MessageType.SPLASH_SCREEN, m -> {
			m.put("mode", Type.ENUM.info(MsgSplashScreen.Mode.values(), MsgSplashScreen.Mode.HIDE));
			m.put("address", Type.STRING.info());
		});
		buildImmutable(props, MessageType.RUN_LUA, m -> m.put("script", Type.FILE.info()));

		PROPERTIES = Collections.unmodifiableMap(props);
	}

	private final MessageType type;
	private final int id;
	private final Map<String, Object> properties;
	private Order dependency;

	public Order(MessageType type) {
		this.type = type;
		this.id = GLOBAL_ID_CNTR++;

		// Set up the properties map with the default values for each entry
		properties = new HashMap<>();
		getPropertyTypes().entrySet().forEach(p -> properties.put(p.getKey(), p.getValue().getDefaultValue()));
	}

	public MessageType getType() {
		return type;
	}

	public int getId() {
		return id;
	}

	public Map<String, Object> getProperties() {
		return properties;
	}

	public Map<String, TypeInfo> getPropertyTypes() {
		return PROPERTIES.get(getType());
	}

	public int getDependencyId() {
		return getDependency() == null ? -1 : getDependency().getId();
	}

	public Order getDependency() {
		return dependency;
	}

	public Message.MetaData getMetadata() {
		return new Message.MetaData(getId(), getDependencyId());
	}

	public void setDependency(Order dependency) {
		this.dependency = dependency;
	}

	@SuppressWarnings("unchecked")
	public <T> T get(String key) {
		return (T) getProperties().get(key);
	}

	@SuppressWarnings("unchecked")
	public <T> T get(@SuppressWarnings("unused") Class<T> clazz, String key) {
		return (T) getProperties().get(key);
	}

	@SuppressWarnings("unused")
	public int getInt(String key) {
		return (Integer) get(key);
	}

	@Override
	public String toString() {
		if (dependency == null)
			return type + "/" + id;
		else
			return type + "/" + id + ", after " + dependency.getId();
	}

	/**
	 * Build an immutable map populated by a callback.
	 * <p>
	 * Writing Ruby in Java since 2017!
	 *
	 * @param properties The property set to add to.
	 * @param key        The key to map into the PROPERTIES map.
	 * @param mapper     A callback to populate a mutable version of the map.
	 */
	private static void buildImmutable(
			Map<MessageType, Map<String, TypeInfo>> properties,
			MessageType key,
			Consumer<Map<String, TypeInfo>> mapper
	) {
		Map<String, TypeInfo> mutable = new HashMap<>();
		mapper.accept(mutable);

		Map<String, TypeInfo> immutable = Collections.unmodifiableMap(mutable);
		properties.put(key, immutable);
	}

	public enum Type {
		STRING(""), FILE(new File(".")), ENUM(0);

		private final Object defaultValue;

		Type(Object defaultValue) {
			this.defaultValue = defaultValue;
		}

		public TypeInfo info() {
			return new TypeInfo(this, null, defaultValue);
		}

		public TypeInfo info(Object data) {
			return new TypeInfo(this, data, defaultValue);
		}

		public TypeInfo info(Object data, Object defaultValue) {
			return new TypeInfo(this, data, defaultValue);
		}
	}

	public static class TypeInfo {
		private final Type type;
		private final Object data;
		private final Object defaultValue;

		public TypeInfo(Type type, Object data, Object defaultValue) {
			this.type = type;
			this.data = data;
			this.defaultValue = defaultValue;
		}

		public Type getType() {
			return type;
		}

		@SuppressWarnings("unchecked")
		public <T> T getData() {
			return (T) data;
		}

		public Object getDefaultValue() {
			return defaultValue;
		}
	}
}
