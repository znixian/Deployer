package xyz.znix.deployer.server.gui;

import xyz.znix.deployer.message.MessageType;
import xyz.znix.deployer.server.Order;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Server GUI
 * <p>
 * Created by znix on 17/02/17.
 */
public class GuiMain {
	private final IGuiServerIO serverIO;

	private JFrame window;

	private JPopupMenu addOrderToStack;
	private JPopupMenu applyBuffer;

	private ItemList inbound;
	private ItemList deployed;
	private ItemList stack;
	private ItemList buffer;

	public static void main(String... args) {
		GuiMain main = new GuiMain(System.out::println);
		main.start();
	}

	public GuiMain(IGuiServerIO serverIO) {
		this.serverIO = serverIO;
	}

	public void start() {
		// Add Order popup menu
		setupOrderMenu();

		// Apply Buffer popup menu
		setupBufferMenu();

		// Setup the JFrame, and all it's contents
		window = new JFrame("Deployer Control Panel");

		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.LINE_AXIS));

		inbound = buildItemList("Inbound", ItemListMode.SELECTOR);
		deployed = buildItemList("Deployed", ItemListMode.INDICATOR);
		buffer = buildItemList("Buffer", ItemListMode.SELECTOR);
		stack = buildItemList("Stack", ItemListMode.CATALOG);

		contentPane.add(inbound);
		contentPane.add(deployed);

		contentPane.add(buffer);
		addPopupButton(buffer, new JButton("Apply"), applyBuffer);

		contentPane.add(stack);
		addPopupButton(stack, new JButton("Add"), addOrderToStack);

		// JFrame settings
		window.setContentPane(contentPane);
		window.pack();
		window.validate();
		window.setBounds(100, 100, 400, 400);
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
		window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // TODO

		// Go!
		window.setVisible(true);
	}

	private ItemList buildItemList(String name, ItemListMode mode) {
		return new ItemList(name, mode, this::getPossibleDepsForOrder, window::repaint);
	}

	private void setupOrderMenu() {
		addOrderToStack = new JPopupMenu();

		for (MessageType type : MessageType.values()) {
			JMenuItem i = new JMenuItem(type.getNiceName());
			addOrderToStack.add(i);
			i.addActionListener(a -> new OrderEditor(new Order(type), stack::addOrder, this::getPossibleDepsForOrder));
		}
	}

	private void setupBufferMenu() {
		applyBuffer = new JPopupMenu();
		JMenuItem i;

		i = new JMenuItem("New Clients");
		i.addActionListener(a -> applyBuffer(true, false));
		applyBuffer.add(i);

		i = new JMenuItem("Current Clients");
		i.addActionListener(a -> applyBuffer(false, true));
		applyBuffer.add(i);

		i = new JMenuItem("Both");
		i.addActionListener(a -> applyBuffer(true, true));
		applyBuffer.add(i);
	}

	/**
	 * Add a button to a {@link ItemList} that opens a popup menu when clicked
	 *
	 * @param list   The list to add to
	 * @param button The button to add
	 * @param menu   The popup menu to open
	 */
	private void addPopupButton(ItemList list, JButton button, JPopupMenu menu) {
		button.addActionListener(a -> menu.show(button, 0, button.getHeight()));
		list.add(button, BorderLayout.SOUTH);
	}

	/**
	 * Move all orders from the buffer stack to the inbound stack and/or to all existing clients.
	 *
	 * @param future  Should the orders be applied to all new clients
	 * @param current Should the orders be applied to all current clients
	 */
	private void applyBuffer(boolean future, boolean current) {
		DefaultListModel<Order> m = buffer.getModel();

		for (int i = 0; i < m.size(); i++) {
			Order order = m.get(i);

			if (future) {
				inbound.addOrder(order);
			}
			if (current) {
				deploy(order);
			}
		}

		m.clear();
	}

	/**
	 * Send a order to all currently connected clients, and add it to the deployed list.
	 * In the case the order has already been sent like this, nothing is done.
	 *
	 * @param order The order to deploy
	 */
	private void deploy(Order order) {
		// Disabled, so that the same command can be sent multiple times
		// If this order has already been sent, do nothing.
		// if (deployed.getModel().contains(order)) return;

		// Add the order to the deployed list. Don't use addOrder, as that moves the order to the
		// top if it is a duplicate
		deployed.getModel().addElement(order);

		// Actually send the command to the clients.
		serverIO.sendToAllClients(order);
	}

	public IOrderProvider getInboundOrderProvider() {
		return this::getInboundOrders;
	}

	private List<Order> getInboundOrders() {
		if (inbound == null) return null;

		DefaultListModel<Order> model = inbound.getModel();
		if (model.isEmpty()) return null;

		List<Order> orders = new ArrayList<>();

		for (int i = 0; i < model.size(); i++) {
			Order order = model.get(i);
			orders.add(order);
		}

		return orders;
	}

	/**
	 * Get all of the orders that could act as a dependency for the supplied order.
	 *
	 * @param order The order to find possible dependencies of.
	 * @return A list of all possible dependencies.
	 */
	private List<Order> getPossibleDepsForOrder(Order order) {
		DefaultListModel<Order> model = stack.getModel();

		List<Order> orders = new ArrayList<>();

		for (int i = 0; i < model.size(); i++) {
			Order dep = model.get(i);
			if (dep != order && dep.getId() < order.getId())
				orders.add(dep);
		}

		return orders;
	}
}
