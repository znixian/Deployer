package xyz.znix.deployer.server;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import xyz.znix.deployer.message.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Takes a order, and turns it into a message
 * <p>
 * Created by znix on 19/02/17.
 */
public class OrderToMessageEncoder extends ChannelOutboundHandlerAdapter {
	private static final Map<MessageType, MessageBuilder> CONSTRUCTORS;

	static {
		Map<MessageType, MessageBuilder> constructors = new HashMap<>();

		constructors.put(MessageType.RUN_COMMAND,
				order -> new MsgRunCommand(order.getMetadata(), order.get("command")));

		constructors.put(MessageType.TORRENT,
				order -> new MsgDownloadTorrent(
						order.getMetadata(),
						Files.readAllBytes(
								order.get(File.class, "torrent").toPath()
						),
						order.get("outdir")
				));

		constructors.put(MessageType.SPLASH_SCREEN,
				order -> new MsgSplashScreen(
						order.getMetadata(),
						order.get("address"),
						order.get("mode")
				));

		constructors.put(MessageType.RUN_LUA,
				order -> new MsgRunLua(
						order.getMetadata(),
						new String(Files.readAllBytes(
								order.get(File.class, "script").toPath()
						), "UTF-8")
				));

		CONSTRUCTORS = Collections.unmodifiableMap(constructors);
	}

	@Override
	public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws IOException {
		Order order = (Order) msg;

		MessageBuilder builder = CONSTRUCTORS.get(order.getType());
		Message message = builder.build(order);

		ctx.write(message, promise);
	}

	private interface MessageBuilder {
		Message build(Order order) throws IOException;
	}
}
