package xyz.znix.deployer.server.gui;

import net.miginfocom.swing.MigLayout;
import xyz.znix.deployer.message.I18n;
import xyz.znix.deployer.server.Order;

import javax.swing.*;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Order Editor, allowing a user to customize an order.
 * <p>
 * Created by znix on 18/02/17.
 */
public class OrderEditor {

	private final Order order;
	private final Consumer<Order> done;
	private final Map<String, JComponent> fields = new HashMap<>();
	private final JFrame window;
	private final Function<Order, List<Order>> orderSupplier;

	private JComboBox<Order> dependencyBox;

	public OrderEditor(Order order, Consumer<Order> done, Function<Order, List<Order>> orderSupplier) {
		this.order = order;
		this.done = done;
		this.orderSupplier = orderSupplier;

		window = new JFrame("Order Editor"); // TODO change title

		// Create our content pane
		JPanel contentPane = new JPanel(new MigLayout(
				"",
				"[][grow]",
				"")
		);
		window.setContentPane(contentPane);

		// Add the title
		JLabel title = new JLabel("Order Editor"); // TODO change text
		title.setHorizontalAlignment(JLabel.CENTER);
		contentPane.add(title, "span");

		// Add the properties
		createPropertyFields();

		// Load the values from the order
		dependencyBox.setSelectedItem(order.getDependency());
		order.getPropertyTypes().entrySet().forEach(entry -> {
			// Find the appropriate field
			JComponent c = fields.get(entry.getKey());

			// Find the value
			Object value = order.getProperties().get(entry.getKey());

			// Depending on the type of field, set the field's value to that of the property
			switch (entry.getValue().getType()) {
				case FILE:
					((FileField) c).setFile((File) value);
					break;
				case STRING:
					((JTextField) c).setText((String) value);
					break;
				case ENUM:
					((JComboBox<Enum>) c).setSelectedItem(value);
					break;
				default:
					throw new UnsupportedOperationException("Unsupported parameter type: " + entry.getValue());
			}
		});

		// Add the OK/Cancel buttons
		createButtonPanel();

		// JFrame settings
		window.pack();
		window.validate();
		window.setBounds(200, 200, 400, 400);
		window.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		// Go!
		window.setVisible(true);
	}

	private void createButtonPanel() {
		// Hold open the body, to put the buttons at the
		// bottom. Grow in the row attributes of contentpane
		// would be better, but we have a unknown number
		// of paramaters/rows.
		window.getContentPane().add(new JPanel(), "span 2, grow, pushy, wrap");

		JPanel buttonPanel = new JPanel(new MigLayout());

		JButton cancel = new JButton(I18n.get("cancel"));
		buttonPanel.add(cancel, "pushx, growx");

		JButton ok = new JButton(I18n.get("ok"));
		buttonPanel.add(ok, "pushx, growx");

		window.getContentPane().add(buttonPanel, "dock south");

		cancel.addActionListener(a -> window.dispose());
		ok.addActionListener(a -> {
			updateOrder();

			done.accept(order);
			window.dispose();
		});
	}

	/**
	 * Add all the property fields so the user can customize this action.
	 */
	private void createPropertyFields() {
		// Cache the types of the properties
		Map<String, Order.TypeInfo> types = order.getPropertyTypes();

		{
			dependencyBox = new JComboBox<>();
			dependencyBox.addItem(null);
			for (Order o : orderSupplier.apply(order)) {
				dependencyBox.addItem(o);
			}
			addField("Dependency", dependencyBox);
		}

		// For each property this order has:
		order.getProperties().entrySet().forEach((Map.Entry<String, Object> entry) -> {

			// Depending on what type of property it is, set c to a different type of field
			// capable of receiving the appropriate type of input.
			JComponent c;
			Order.TypeInfo typeInfo = types.get(entry.getKey());
			Order.Type type = typeInfo.getType();
			switch (type) {
				case STRING:
					c = new JTextField();
					break;
				case FILE:
					c = new FileField();
					break;
				case ENUM: {
					JComboBox<Enum> jc = new JComboBox<>();
					for (Enum mode : (Enum[]) typeInfo.getData()) {
						jc.addItem(mode);
					}
					c = jc;
					break;
				}
				default:
					throw new UnsupportedOperationException("Unsupported parameter type: " + type);
			}

			// Store that field in the fields map for later use
			fields.put(entry.getKey(), c);

			addField(entry.getKey(), c);
		});
	}

	private void addField(String name, JComponent component) {
		// Add a label
		window.getContentPane().add(new JLabel(name));

		// Add that field next to the label, filling the right-hand side of the screen
		window.getContentPane().add(component, "wrap, growx");
	}

	/**
	 * Take the property values the user has entered, and move
	 * them into the order object.
	 */
	@SuppressWarnings("unchecked")
	private void updateOrder() {
		// For each property
		order.getPropertyTypes().entrySet().forEach(entry -> {
			// Find the appropriate field
			JComponent c = fields.get(entry.getKey());

			// Somewhere to store the value
			Object value;

			// Depending on the type of field, read the field's value into the value variable
			switch (entry.getValue().getType()) {
				case FILE:
					value = ((FileField) c).getFile();
					break;
				case STRING:
					value = ((JTextField) c).getText();
					break;
				case ENUM:
					value = ((JComboBox<Enum>) c).getSelectedItem();
					break;
				default:
					throw new UnsupportedOperationException("Unsupported parameter type: " + entry.getValue());
			}

			// Copy this value into the order itself.
			order.getProperties().put(entry.getKey(), value);
		});

		order.setDependency((Order) dependencyBox.getSelectedItem());
	}
}
