package xyz.znix.deployer.server;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import xyz.znix.deployer.server.gui.IOrderProvider;

import java.io.IOException;
import java.util.List;

/**
 * Handles newly-connected clients
 * <p>
 * Created by znix on 17/02/17.
 */
public class ConnectingClientHandler extends ChannelInboundHandlerAdapter {
	private final IOrderProvider orderSupplier;
	private final ChannelGroup clients;

	public ConnectingClientHandler(IOrderProvider orderSupplier, ChannelGroup clients) {
		this.orderSupplier = orderSupplier;
		this.clients = clients;
	}

	@Override
	public void channelActive(final ChannelHandlerContext ctx) throws IOException {
		clients.add(ctx.channel());

		List<Order> orders = orderSupplier.getOrders();
		if (orders != null) {
			for (Order order : orders) {
				ctx.write(order);
			}
			ctx.flush();
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}
}
