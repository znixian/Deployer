package xyz.znix.deployer.server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import xyz.znix.deployer.message.Message;

/**
 * Translates a message into bytes
 * <p>
 * Created by znix on 17/02/17.
 */
public class MessageEncoder extends MessageToByteEncoder<Message> {
	@Override
	protected void encode(ChannelHandlerContext ctx, Message msg, ByteBuf out) {
		out.writeInt(msg.getId());
		out.writeInt(msg.getType().ordinal());
		out.writeInt(msg.getDependency());
		msg.write(out);
	}
}
