package xyz.znix.deployer.server.gui;

import xyz.znix.deployer.server.Order;

import javax.swing.*;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/**
 * Represents a type of ItemList
 *
 * Created by znix on 18/02/17.
 */
public enum ItemListMode {
	INDICATOR(false, false), SELECTOR(true, false) {
		@Override
		protected void setup(ItemList target) {
			super.setup(target);
			ItemListMode.setupDeleteKey(target);
		}
	}, CATALOG(false, true);

	private final boolean in, out;

	ItemListMode(boolean in, boolean out) {
		this.in = in;
		this.out = out;
	}

	protected void setup(ItemList target) {
		JList<Order> list = target.getList();
		list.setDragEnabled(true);
		list.setTransferHandler(new TransferHandler() {

			public boolean canImport(TransferHandler.TransferSupport info) {
				// Only accept orders
				return in && info.isDataFlavorSupported(ItemList.ORDER_FLAVOR);
			}

			public boolean importData(TransferHandler.TransferSupport info) {
				if (!in) {
					return false;
				}

				if (!info.isDrop()) {
					return false;
				}

				// Check for String flavor
				if (!info.isDataFlavorSupported(ItemList.ORDER_FLAVOR)) {
					// throw new RuntimeException("Cannot accept data!");
					return false;
				}

				// Get the string that is being dropped.
				Transferable t = info.getTransferable();
				Order order;
				try {
					order = (Order) t.getTransferData(ItemList.ORDER_FLAVOR);
				} catch (Exception e) {
					return false;
				}

				target.addOrder(order);

				return true;
			}

			public int getSourceActions(JComponent c) {
				return out ? COPY : NONE;
			}

			protected Transferable createTransferable(JComponent c) {
				return out ? new OrderSelection(list.getSelectedValue()) : null;
			}
		});
		list.setDropMode(DropMode.ON_OR_INSERT);
	}

	/**
	 * Allow the backspace key to remove items from a list
	 *
	 * @param target The list to affect
	 */
	private static void setupDeleteKey(ItemList target) {
		final String MAPPING = "removeItem";

		JList<Order> list = target.getList();
		list.getInputMap().put(
				KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0),
				MAPPING);

		list.getActionMap().put(MAPPING, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Order value = list.getSelectedValue();
				if (value == null) return;
				target.getModel().removeElement(value);
			}
		});
	}
}
