package xyz.znix.deployer.server.gui;

import xyz.znix.deployer.server.Order;

/**
 * This is implemented by the server to allow the GUI to send orders to it.
 * This allows the GUI to be tested separately from the rest of the server.
 * Allows the server and it's GUI to communicate with each other.
 * <p>
 * Created by znix on 18/02/17.
 */
public interface IGuiServerIO {
	void sendToAllClients(Order order);
}
