package xyz.znix.deployer.server.gui;

import xyz.znix.deployer.message.I18n;
import xyz.znix.deployer.server.Order;

import java.util.ArrayList;
import java.util.List;
import java.awt.*;
import javax.swing.*;

/**
 * Renderer to nicely display an order when in a list
 * <p>
 * Created by znix on 18/02/17.
 */
public class OrderRenderer extends JPanel implements ListCellRenderer<Order> {
	private static final Color BG_EVEN = new Color(168, 168, 168);
	private static final Color BG_ODD = new Color(190, 190, 190);

	private final JLabel type;
	private final JPanel values;
	private final List<JLabel> labelPool;
	private int poolIndex;

	public OrderRenderer() {
		setLayout(new BorderLayout());

		type = new JLabel();
		type.setHorizontalAlignment(SwingConstants.CENTER);
		add(type, BorderLayout.NORTH);

		values = new JPanel();
		values.setLayout(new BoxLayout(values, BoxLayout.Y_AXIS));
		values.setOpaque(false);
		add(values, BorderLayout.CENTER);

		labelPool = new ArrayList<>();
	}

	@Override
	public Component getListCellRendererComponent(
			JList<? extends Order> list, Order value,
			int index, boolean isSelected, boolean cellHasFocus
	) {
		// Start from the start of the pool again.
		poolIndex = 0;

		type.setText(value.getType().getNiceName() + " (" + value.getId() + ")");

		// Colour in background
		if (isSelected) {
			setBackground(Color.BLUE);
		} else {
			boolean even = index % 2 == 1;
			setBackground(even ? BG_EVEN : BG_ODD);
		}

		// Clear out the values
		values.removeAll();

		// Add dependency label
		{
			int did = value.getDependencyId();
			String val = did == -1 ? I18n.get("none") : "" + did;
			values.add(assignLabel(I18n.get("dependency") + ": " + val));
		}

		// Add all the properties in name: value format
		value.getProperties().entrySet().stream()
				.map(e -> e.getKey() + ": " + e.getValue())
				.map(this::assignLabel)
				.forEach(values::add);

		return this;
	}

	private JLabel assignLabel(String text) {
		if (poolIndex >= labelPool.size()) {
			labelPool.add(new JLabel());
		}

		JLabel label = labelPool.get(poolIndex++);
		label.setText(text);

		return label;
	}
}
