package xyz.znix.deployer.server.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.function.Consumer;

/**
 * A text field for entering file paths, along with a 'choose' choose.
 * <p>
 * Created by znix on 18/02/17.
 */
public class FileField extends JPanel {

	private JTextField path;

	private JFileChooser fileChooser;

	/**
	 * Create a new FileField, using the default settings.
	 */
	public FileField() {
		fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File("."));

		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

		add(path = new JTextField());

		JButton choose = new JButton("Choose");
		choose.addActionListener(this::chooseAction);
		add(choose);
	}

	/**
	 * Create a new FileField.
	 *
	 * @param customizer A callback run on this object, to make inline customization easier.
	 */
	public FileField(Consumer<FileField> customizer) {
		this();
		customizer.accept(this);
	}

	private void chooseAction(@SuppressWarnings("unused") ActionEvent evt) {
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			path.setText(fileChooser.getSelectedFile().getAbsolutePath());
		}
	}

	/**
	 * Get the currently selected file.
	 *
	 * @return The currently selected file.
	 */
	public File getFile() {
		return new File(path.getText());
	}

	/**
	 * Set the currently selected file. There are no checks on
	 * whether the file exists or not, though.
	 *
	 * @param file The file to select.
	 */
	public void setFile(File file) {
		path.setText(file.getAbsolutePath());
	}

	/**
	 * Get the underlying file chooser. Useful for customization.
	 *
	 * @return The JFileChooser
	 */
	public JFileChooser getFileChooser() {
		return this.fileChooser;
	}
}
